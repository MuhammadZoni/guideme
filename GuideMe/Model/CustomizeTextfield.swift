//
//  CustomizeTextfield.swift
//  GuideMe
//
//  Created by apple on 2/21/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class CustomizeTextfield: UITextField {

    let padding = UIEdgeInsets(top: 0, left: 25, bottom: 0, right: 5)
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
}
