//
//  Constant.swift
//  GuideMe
//
//  Created by apple on 2/21/20.
//  Copyright © 2020 apple. All rights reserved.
//

import Foundation

//Guider Data
var guiderfname:String = ""
var guiderlname:String = ""
var guideremail:String = ""
var guiderpassword:String = ""
var guiderphoneNumber:String = ""
var guidernationalID:String = ""
var guidercity:String = ""
var guiderdob:String = ""
var guiderexperience:String = ""
var guiderperHour:String = ""
var guiderCarNumber:String = ""
var guiderCarModel:String = ""
var guiderCarType:String = ""
