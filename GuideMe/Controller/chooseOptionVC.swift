//
//  chooseOptionVC.swift
//  GuideMe
//
//  Created by apple on 2/18/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class chooseOptionVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var signupBtn: UIButton!
    @IBOutlet weak var guiderBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        loginBtn.layer.cornerRadius = 5
        loginBtn.layer.borderWidth = 1
        loginBtn.layer.borderColor = UIColor.black.cgColor
        
        signupBtn.layer.cornerRadius = 5
        signupBtn.layer.borderWidth = 1
        signupBtn.layer.borderColor = UIColor.black.cgColor
        
        guiderBtn.layer.cornerRadius = 5
        guiderBtn.layer.borderWidth = 1
        guiderBtn.layer.borderColor = UIColor.black.cgColor
    }

}
