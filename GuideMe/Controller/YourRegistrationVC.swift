//
//  YourRegistrationVC.swift
//  GuideMe
//
//  Created by apple on 2/20/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit
import SVProgressHUD
import AlamofireImage
import Alamofire
import SwiftyJSON


class YourRegistrationVC: UIViewController {

    var id = ""
    var name = ""
    var phone = ""
    var mail = ""
    
    @IBOutlet weak var lblMail: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        nameLbl.text = name
        lblMail.text = mail
        lblPhone.text = phone
        
    }
    @IBAction func tappedBackBtn(_ sender: Any) {
          let home = self.storyboard?.instantiateViewController(withIdentifier: "home") as! tabbarViewController
        self.present(home, animated: false, completion: nil)
    }
    @IBAction func tappedCannelReservation(_ sender: Any) {
       alert()
    }
}

extension YourRegistrationVC{
    func cannelReservations(){

           SVProgressHUD.show()

                   let param:Parameters = ["id":id]
                   Alamofire.request("http://app.a2zhandymanuae.com/api/cancelReservation.php", method: .post, parameters: param).responseData { response in
                       switch response.result{
                       case.success(let value):
                           let json = JSON(value)
                           let data = json["status"].boolValue
                           
                           if data{
                               let home = self.storyboard?.instantiateViewController(withIdentifier: "home") as! tabbarViewController
                               self.present(home, animated: false, completion: nil)
                           } else {
                               AlertHelper.displayAlertMessage(messageToDisplay: "Invalid", sender: self)
                           }
                           SVProgressHUD.dismiss()
                           break

                       case.failure(let error):
                           print(error.localizedDescription)
                       }
                   }
               
               
       }
}

extension YourRegistrationVC{
    func alert() {
        
        let alerts = UIAlertController.init(title: "Are you Sure Cannel Revervations??" , message: "", preferredStyle: .alert)
               alerts.addAction(UIAlertAction(title: "YES", style: .default, handler: { (uiaction:UIAlertAction) in
                self.cannelReservations()
               }))
        
        
        alerts.addAction(UIAlertAction(title: "NO", style: .default, handler: { (uiaction:UIAlertAction) in
                       
                      }))
        
               self.present(alerts, animated: true, completion: nil)
        
    }
}
