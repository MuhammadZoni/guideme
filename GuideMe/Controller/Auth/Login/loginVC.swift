//
//  loginVC.swift
//  GuideMe
//
//  Created by apple on 2/18/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD


class loginVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var confirmBtn: UIButton!
    @IBOutlet weak var guiderCheckBtn: customizeButton!
    @IBOutlet weak var userCheckBtn: customizeButton!
    
    //MARK:- Variables
    var d = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        style()
        
    }
    //MARK:- Outlets
    @IBAction func guiderBtn(_ sender: UIButton) {
        userOrGuider = 1
        d = "a"
        guiderCheckBtn.setImage(UIImage(named: "check"), for: .normal)
        userCheckBtn.setImage(UIImage(named: ""), for: .normal)
        
    }
    @IBAction func userBtn(_ sender: UIButton) {
        userOrGuider = 2
        d = "b"
        userCheckBtn.setImage(UIImage(named: "check"), for: .normal)
        guiderCheckBtn.setImage(UIImage(named: ""), for: .normal)
    }
    
    
    @IBAction func tappedConfirmBtn(_ sender: Any) {
        if emailTF.text == "" || passwordTF.text == "" || d == ""{
            AlertHelper.displayAlertMessage(messageToDisplay: "All fields are necessary to fill", sender: self)
        } else {
            SVProgressHUD.show()
            if userOrGuider == 1{
                let param:Parameters = ["email":emailTF.text!,
                                        "password":passwordTF.text!]
                Alamofire.request("http://app.a2zhandymanuae.com/api/signInGuider.php", method: .post, parameters: param).responseData { response in
                    switch response.result{
                    case.success(let value):
                        let json = JSON(value)
                        let data = json["action"].stringValue
                        
                        if data == "Guider logged in"{
                            SVProgressHUD.dismiss()
                            
                            let verified = json["data"]["verified"].stringValue
                            
                            if verified == "1"{
                                password = self.passwordTF.text!
                                 guiderId = json["data"]["id"].intValue
                                let home = self.storyboard?.instantiateViewController(withIdentifier: "home") as! tabbarViewController
                                self.present(home, animated: false, completion: nil)
                            }else{
                                AlertHelper.displayAlertMessage(messageToDisplay: "Your Account is Waiting For Review ", sender: self)
                            }
                          
                        } else {
                            SVProgressHUD.dismiss()
                            AlertHelper.displayAlertMessage(messageToDisplay: "Invalid email or password", sender: self)
                        }
                    case.failure(let error):
                        print(error.localizedDescription)
                    }
                }
            } else if userOrGuider == 2{
                
                let param:Parameters = ["email":emailTF.text!,
                                        "password":passwordTF.text!]
                Alamofire.request("http://app.a2zhandymanuae.com/api/signInUser.php", method: .post, parameters: param).responseData { response in
                    switch response.result{
                    case.success(let value):
                        let json = JSON(value)
                        let data = json["action"].stringValue
                        
                        if data == "User logged in"{
                            
                            userId = json["data"].intValue
                            SVProgressHUD.dismiss()
                             let home = self.storyboard?.instantiateViewController(withIdentifier: "home") as! tabbarViewController
                                          self.present(home, animated: false, completion: nil)
                           
                        } else {
                            SVProgressHUD.dismiss()
                            AlertHelper.displayAlertMessage(messageToDisplay: "Invalid email or password", sender: self)
                        }
                    case.failure(let error):
                        print(error.localizedDescription)
                    }
                }
            } else {
                AlertHelper.displayAlertMessage(messageToDisplay: "Select either you are user or guider", sender: self)
            }
        }
    }
    @IBAction func tappedForgotPasswordBtn(_ sender: Any) {
         self.performSegue(withIdentifier: "forgot", sender: self)
    }
    
    @IBAction func tappedBackBtn(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
}

