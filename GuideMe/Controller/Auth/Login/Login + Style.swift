//
//  Login + Style.swift
//  GuideMe
//
//  Created by user on 22/02/2020.
//  Copyright © 2020 apple. All rights reserved.
//

import Foundation
import UIKit

extension loginVC{
func style(){
    hideKeyboard()
           emailTF.layer.cornerRadius = 8.0
           passwordTF.layer.cornerRadius = 8.0
           confirmBtn.layer.cornerRadius = 8.0
           emailTF.layer.borderWidth = 1
           emailTF.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
           passwordTF.layer.borderWidth = 1
           passwordTF.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
          
          
           emailTF.attributedPlaceholder = NSAttributedString(string: "Enter Email",
                                                           attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)])
           passwordTF.attributedPlaceholder = NSAttributedString(string: "Enter Password",
           attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)])
}
}
