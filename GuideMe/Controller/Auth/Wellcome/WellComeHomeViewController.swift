//
//  WellComeHomeViewController.swift
//  GuideMe
//
//  Created by user on 21/02/2020.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class WellComeHomeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

       
        
    }
    
    @IBAction func tappedSignInBtn(_ sender: Any) {
        self.performSegue(withIdentifier: "signin", sender: self)
    }
    @IBAction func tappedSignUpBtn(_ sender: Any) {
        self.performSegue(withIdentifier: "visitor", sender: self)
    }
    
    @IBAction func tappedSignUpASGuiderBtn(_ sender: Any) {
        self.performSegue(withIdentifier: "guider", sender: self)
    }
    
}
