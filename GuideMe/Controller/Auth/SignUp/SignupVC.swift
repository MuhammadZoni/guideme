//
//  SignupVC.swift
//  GuideMe
//
//  Created by apple on 2/18/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD

class SignupVC: UIViewController {

    let pickerView = UIPickerView()
    var datePicker : UIDatePicker?
    
    //MARK:- Outlets
    @IBOutlet weak var firstnameTF: UITextField!
    @IBOutlet weak var lastnameTF: UITextField!
    @IBOutlet weak var emailTf: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var phoneTF: UITextField!
    @IBOutlet weak var countryTF: UITextField!
    @IBOutlet weak var dobTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        style()
    }
    
    @IBAction func tappedSignUpBtn(_ sender: Any) {
        if firstnameTF.text == "" || lastnameTF.text == "" || emailTf.text == "" || passwordTF.text == "" || phoneTF.text == "" || countryTF.text == "" || dobTF.text == "" {
            AlertHelper.displayAlertMessage(messageToDisplay: "All fields are necessary to fill", sender: self)
        } else {
            SVProgressHUD.show()
            let param:Parameters = ["firstName":firstnameTF.text!,
                                    "lastName":lastnameTF.text!,
                                    "country":countryTF.text!,
                                    "email":emailTf.text!,
                                    "phone":phoneTF.text!,
                                    "dob":dobTF.text!,
                                    "password":passwordTF.text!,
            ]
            Alamofire.request("http://app.a2zhandymanuae.com/api/signUpUser.php", method: .post, parameters: param).responseData { response in
                switch response.result{
                case.success(let value):
                    let json = JSON(value)
                    let status = json["status"].boolValue
                    if status{
                        SVProgressHUD.dismiss()
                        userOrGuider = 2
                        userId = json["data"].intValue
                        let home = self.storyboard?.instantiateViewController(withIdentifier: "home") as! tabbarViewController
                        self.present(home, animated: false, completion: nil)
                    }else{
                        SVProgressHUD.dismiss()
                        AlertHelper.displayAlertMessage(messageToDisplay: json["action"].stringValue, sender: self)
                    }
                    
                    print(json)
                   
                case.failure(let error):
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    @IBAction func tappedSignInBtn(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    @IBAction func tappedBackBtn(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func tapped(tapGester:UITapGestureRecognizer)
       {
           view.endEditing(true)
       }
       
       @objc func dateChange(datePicker : UIDatePicker )
       {
           let dateFormater = DateFormatter()
           dateFormater.dateFormat = "MM/dd/yyyy"
           dobTF.text = dateFormater.string(from: datePicker.date)
           view.endEditing(true)
       }
}



