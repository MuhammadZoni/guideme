//
//  SignUP + Style .swift
//  GuideMe
//
//  Created by user on 22/02/2020.
//  Copyright © 2020 apple. All rights reserved.
//

import Foundation
import UIKit

extension SignupVC{
func style(){
    
    hideKeyboard()
    datePicker = UIDatePicker()
    datePicker?.datePickerMode = .date
    datePicker?.addTarget(self, action: #selector(SignupVC.dateChange(datePicker:)), for: .valueChanged)
    dobTF.inputView = datePicker
    
           firstnameTF.layer.cornerRadius = 8.0
           lastnameTF.layer.cornerRadius = 8.0
           emailTf.layer.cornerRadius = 8.0
           passwordTF.layer.cornerRadius = 8.0
           phoneTF.layer.cornerRadius = 8.0
           countryTF.layer.cornerRadius = 8.0
           dobTF.layer.cornerRadius = 8.0
    
           firstnameTF.layer.borderWidth = 1
           firstnameTF.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
           lastnameTF.layer.borderWidth = 1
           lastnameTF.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
           emailTf.layer.borderWidth = 1
           emailTf.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
           passwordTF.layer.borderWidth = 1
           passwordTF.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
           phoneTF.layer.borderWidth = 1
          phoneTF.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
         countryTF.layer.borderWidth = 1
         countryTF.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
         dobTF.layer.borderWidth = 1
         dobTF.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
          
          
           firstnameTF.attributedPlaceholder = NSAttributedString(string: "Enter First Name",
                                                           attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)])
           lastnameTF.attributedPlaceholder = NSAttributedString(string: "Enter Last Name",
           attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)])
    emailTf.attributedPlaceholder = NSAttributedString(string: "Enter Email",
                                                            attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)])
            passwordTF.attributedPlaceholder = NSAttributedString(string: "Enter Password",
            attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)])
    phoneTF.attributedPlaceholder = NSAttributedString(string: "Enter Phone Number",
                                                            attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)])
            countryTF.attributedPlaceholder = NSAttributedString(string: "Enter Country",
            attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)])
    dobTF.attributedPlaceholder = NSAttributedString(string: "Enter Date Of Birth",
                                                            attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)])
       
}
}

