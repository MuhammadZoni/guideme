//
//  GuiderSignupVC.swift
//  GuideMe
//
//  Created by apple on 2/18/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class GuiderSignupVC: UIViewController {

    let pickerView = UIPickerView()
    var datePicker : UIDatePicker?
    //MAKR:- Outlets
    @IBOutlet weak var fNameTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var lNameTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var phoneTF: UITextField!
    @IBOutlet weak var IDNumberTF: UITextField!
    @IBOutlet weak var citytTF: UITextField!
    @IBOutlet weak var dobTF: UITextField!
    @IBOutlet weak var experienceTF: UITextField!
    @IBOutlet weak var perHourTF: UITextField!
    @IBOutlet weak var yesCheckbox: UIButton!
    @IBOutlet weak var nocheckbox: UIButton!
    
    //MARK:- Variables
    var isHavingCar:Int = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
       style()
        
    }
    
    @IBAction func yesBtn(_ sender: UIButton) {
        isHavingCar = 1
        yesCheckbox.setImage(UIImage(named: "check"), for: .normal)
        nocheckbox.setImage(UIImage(named: ""), for: .normal)
    }
    @IBAction func noBtn(_ sender: UIButton) {
        isHavingCar = 0
        yesCheckbox.setImage(UIImage(named: ""), for: .normal)
        nocheckbox.setImage(UIImage(named: "check"), for: .normal)
    }
    @IBAction func tappedConfirmBtn(_ sender: Any) {
        if fNameTF.text ==  "" || emailTF.text ==  "" || lNameTF.text ==  "" || passwordTF.text ==  "" || phoneTF.text ==  "" || IDNumberTF.text ==  "" || citytTF.text ==  "" || dobTF.text ==  "" || experienceTF.text ==  "" || perHourTF.text ==  ""{
            AlertHelper.displayAlertMessage(messageToDisplay: "All fields are necessary to fill", sender: self)
        } else {
            if isHavingCar == -1{
                AlertHelper.displayAlertMessage(messageToDisplay: "Please select either you have car or not", sender: self)
            } else if isHavingCar == 0{
                guiderfname = fNameTF.text!
                guiderlname = lNameTF.text!
                guideremail = emailTF.text!
                guiderpassword = passwordTF.text!
                guiderphoneNumber = phoneTF.text!
                guidernationalID = IDNumberTF.text!
                guidercity = citytTF.text!
                guiderdob = dobTF.text!
                guiderexperience = experienceTF.text!
                guiderperHour = perHourTF.text!
                self.performSegue(withIdentifier: "daysSegue", sender: self)
            } else if isHavingCar == 1{
                guiderfname = fNameTF.text!
                guiderlname = lNameTF.text!
                guideremail = emailTF.text!
                guiderpassword = passwordTF.text!
                guiderphoneNumber = phoneTF.text!
                guidernationalID = IDNumberTF.text!
                guidercity = citytTF.text!
                guiderdob = dobTF.text!
                guiderexperience = experienceTF.text!
                guiderperHour = perHourTF.text!
                self.performSegue(withIdentifier: "carInfo", sender: self)
                print(guideremail)
            }
        }
    }
    
    @IBAction func tappedSignInBtn(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
       @objc func tapped(tapGester:UITapGestureRecognizer)
          {
              view.endEditing(true)
          }
          
          @objc func dateChange(datePicker : UIDatePicker )
          {
              let dateFormater = DateFormatter()
              dateFormater.dateFormat = "MM/dd/yyyy"
              dobTF.text = dateFormater.string(from: datePicker.date)
              view.endEditing(true)
          }
}
