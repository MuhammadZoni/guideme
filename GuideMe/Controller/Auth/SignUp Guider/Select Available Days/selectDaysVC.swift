//
//  selectDaysVC.swift
//  GuideMe
//
//  Created by apple on 2/18/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD

class selectDaysVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:- Arrays
    var daysOfWeek:[String] = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Everyday"]
    var selectedDays:[String] = []
    override func viewDidLoad() {
        super.viewDidLoad()

      
    }
  
    @IBAction func tappedNextBtn(_ sender: Any) {
        
        if selectedDays.count != 0{
            SVProgressHUD.show()
            let sepratedArray = selectedDays.joined(separator: ",")
            let param:Parameters = ["firstName":guiderfname,"lastName":guiderlname,"nid":guidernationalID,"email":guideremail,"phone":guiderphoneNumber,"city":guidercity,"dob":guiderdob,"password":guiderpassword,"experience":guiderexperience,"carType":guiderCarType,"carModel":guiderCarModel,"carPlate":guiderCarNumber,"perHour":guiderperHour,"availability":sepratedArray]
            Alamofire.request("http://app.a2zhandymanuae.com/api/signUpGuider.php", method: .post, parameters: param).responseData { response in
                switch response.result{
                case.success(let value):
                    let json = JSON(value)
                    print(json)
                    SVProgressHUD.dismiss()
                    self.alert()
                    
                    
                case.failure(let error):
                    print(error.localizedDescription)
                }
            }
        } else {
            AlertHelper.displayAlertMessage(messageToDisplay: "Please select days of availability", sender: self)
        }
        
        //self.performSegue(withIdentifier: "hoursSegue", sender: self)
    }
    
    @IBAction func tappedBackBtn(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
}
extension selectDaysVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return daysOfWeek.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "selectDaysTVCell", for: indexPath) as! selectDaysTVCell
        cell.nameOfDay.text = daysOfWeek[indexPath.row]
        
        cell.selectBtn = {
            if self.selectedDays.contains(self.daysOfWeek[indexPath.row]){
                self.selectedDays.remove(at: self.selectedDays.firstIndex(of: self.daysOfWeek[indexPath.row])!)
                cell.checkBtn.setImage(UIImage(named: ""), for: .normal)
                cell.checkBtn.backgroundColor = #colorLiteral(red: 0.9333333333, green: 0.9333333333, blue: 0.9333333333, alpha: 1)
            } else {
                self.selectedDays.append(self.daysOfWeek[indexPath.row])
                    cell.checkBtn.setImage(UIImage(named: "tick"), for: .normal)
                    cell.checkBtn.backgroundColor = #colorLiteral(red: 0.9607843161, green: 0.7058823705, blue: 0.200000003, alpha: 1)
                    cell.checkBtn.tintColor = UIColor.white
                }
            }
        
        cell.checkBtn.layer.cornerRadius = 2
        cell.checkBtn.layer.borderWidth = 1
        cell.checkBtn.layer.borderColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}

extension selectDaysVC{
    func alert() {
        
        let alerts = UIAlertController.init(title: "Registeration Successfully" , message: "Your Account Waiting For Review .After One Hour You Can check Your Email For Account Status(Approve or not Approve)", preferredStyle: .alert)
               alerts.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (uiaction:UIAlertAction) in
                  let LoginScreen = self.storyboard?.instantiateViewController(withIdentifier: "login") as! WellComeHomeViewController
                              self.present(LoginScreen, animated: false, completion: nil)
               }))
               self.present(alerts, animated: true, completion: nil)
        
    }
}
