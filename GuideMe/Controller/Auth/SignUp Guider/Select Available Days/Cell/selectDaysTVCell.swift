//
//  selectDaysTVCell.swift
//  GuideMe
//
//  Created by apple on 2/18/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class selectDaysTVCell: UITableViewCell {

    @IBOutlet weak var nameOfDay: UILabel!
    @IBOutlet weak var checkBtn: UIButton!
    
    var selectBtn: (() -> Void)? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    @IBAction func btnClicked(_ sender: UIButton) {
        selectBtn?()
    }
    
}
