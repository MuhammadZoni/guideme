//
//  CarInfo + Style.swift
//  GuideMe
//
//  Created by user on 22/02/2020.
//  Copyright © 2020 apple. All rights reserved.
//

import Foundation
import UIKit


extension CarInfoViewController{
func style(){
           numberPlateTF.layer.cornerRadius = 8.0
           modelTF.layer.cornerRadius = 8.0
           typeTF.layer.cornerRadius = 8.0
          
    
           numberPlateTF.layer.borderWidth = 1
           numberPlateTF.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
           modelTF.layer.borderWidth = 1
           modelTF.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
           typeTF.layer.borderWidth = 1
           typeTF.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
           
          
          
           numberPlateTF.attributedPlaceholder = NSAttributedString(string: "Enter Car Number Plate",
                                                           attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)])
           modelTF.attributedPlaceholder = NSAttributedString(string: "Enter Car Model",
           attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)])
    typeTF.attributedPlaceholder = NSAttributedString(string: "Enter Car type",
                                                            attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)])
   
}
}
