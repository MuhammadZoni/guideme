//
//  CarInfoViewController.swift
//  GuideMe
//
//  Created by user on 21/02/2020.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class CarInfoViewController: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var numberPlateTF: UITextField!
    @IBOutlet weak var modelTF: UITextField!
    @IBOutlet weak var typeTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        style()
        
        
    }
    @IBAction func tappedConfirmBtn(_ sender: Any) {
        if numberPlateTF.text == "" || modelTF.text == "" || typeTF.text == ""{
            AlertHelper.displayAlertMessage(messageToDisplay: "All fields are necessary to fill", sender: self)
        } else{
            guiderCarNumber = numberPlateTF.text!
            guiderCarModel = modelTF.text!
            guiderCarType = typeTF.text!
            self.performSegue(withIdentifier: "daysSegue", sender: self)
            print(guiderCarNumber)
        }
    }
    @IBAction func tappedBackBtn(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
}
