//
//  SignUpGuider+Style.swift
//  GuideMe
//
//  Created by user on 22/02/2020.
//  Copyright © 2020 apple. All rights reserved.
//

import Foundation
import UIKit

extension GuiderSignupVC{
func style(){
    hideKeyboard()
    datePicker = UIDatePicker()
    datePicker?.datePickerMode = .date
    datePicker?.addTarget(self, action: #selector(GuiderSignupVC.dateChange(datePicker:)), for: .valueChanged)
    dobTF.inputView = datePicker
    
           fNameTF.layer.cornerRadius = 8.0
           lNameTF.layer.cornerRadius = 8.0
           emailTF.layer.cornerRadius = 8.0
           passwordTF.layer.cornerRadius = 8.0
           phoneTF.layer.cornerRadius = 8.0
           IDNumberTF.layer.cornerRadius = 8.0
           dobTF.layer.cornerRadius = 8.0
           citytTF.layer.cornerRadius = 8.0
           experienceTF.layer.cornerRadius = 8.0
           perHourTF.layer.cornerRadius = 8.0
    
           fNameTF.layer.borderWidth = 1
           fNameTF.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
           lNameTF.layer.borderWidth = 1
           lNameTF.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
           emailTF.layer.borderWidth = 1
           emailTF.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
           passwordTF.layer.borderWidth = 1
           passwordTF.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
           phoneTF.layer.borderWidth = 1
           phoneTF.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
          IDNumberTF.layer.borderWidth = 1
          IDNumberTF.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
           dobTF.layer.borderWidth = 1
         dobTF.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    citytTF.layer.borderWidth = 1
     citytTF.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    experienceTF.layer.borderWidth = 1
    experienceTF.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    perHourTF.layer.borderWidth = 1
    perHourTF.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
          
          
           fNameTF.attributedPlaceholder = NSAttributedString(string: "Enter First Name",
                                                           attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)])
           lNameTF.attributedPlaceholder = NSAttributedString(string: "Enter Last Name",
           attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)])
    
    emailTF.attributedPlaceholder = NSAttributedString(string: "Enter Email",
                                                            attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)])
            passwordTF.attributedPlaceholder = NSAttributedString(string: "Enter Password",
            attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)])
    phoneTF.attributedPlaceholder = NSAttributedString(string: "Enter Phone Number",
                                                            attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)])
            IDNumberTF.attributedPlaceholder = NSAttributedString(string: "Enter National Identity Number",
            attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)])
    dobTF.attributedPlaceholder = NSAttributedString(string: "Enter Date Of Birth",
                                                            attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)])
    
    citytTF.attributedPlaceholder = NSAttributedString(string: "Enter City",
                                                              attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)])
              experienceTF.attributedPlaceholder = NSAttributedString(string: "Enter Your Experience (years)",
              attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)])
      perHourTF.attributedPlaceholder = NSAttributedString(string: "Enter Per Hour Charges",
                                                              attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)])
    
    
    
    
    
    
    
       
}
}

