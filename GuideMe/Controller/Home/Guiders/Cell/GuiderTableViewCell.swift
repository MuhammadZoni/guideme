//
//  GuiderTableViewCell.swift
//  GuideMe
//
//  Created by user on 22/02/2020.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class GuiderTableViewCell: UITableViewCell {

    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var star1: UIImageView!
        @IBOutlet weak var star2: UIImageView!
        @IBOutlet weak var star3: UIImageView!
        @IBOutlet weak var star4: UIImageView!
        @IBOutlet weak var star5: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
