//
//  GuidersViewController.swift
//  GuideMe
//
//  Created by user on 22/02/2020.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit
import SVProgressHUD
import SwiftyJSON
import AlamofireImage

class GuidersViewController: UIViewController {

    var id = ""
    var cityNames = ""
    var selectedIndex = 0
    
    @IBOutlet weak var empty: UILabel!
    var guidersArr = [guiders]()
    @IBOutlet weak var cityName: UILabel!
    @IBOutlet weak var tblVU: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        SVProgressHUD.show()
        let color = #colorLiteral(red: 0.1607843137, green: 0.6823529412, blue: 0.2901960784, alpha: 1)
        SVProgressHUD.setForegroundColor(color)
        guidersArr.removeAll()
        getGuiders()
        cityName.text = cityNames
        empty.isHidden = true
    }
    @IBAction func tappedBackBtn(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
}
extension GuidersViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return guidersArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblVU.dequeueReusableCell(withIdentifier: "guiders") as! GuiderTableViewCell
        
        
        cell.name.text = guidersArr[indexPath.row].name
        if guidersArr[indexPath.row].image == ""{
            
        }else{
            cell.img.af_setImage(withURL: URL.init(string: guidersArr[indexPath.row].image!)!)
        }
        cell.img.layer.cornerRadius = 8.0
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        self.performSegue(withIdentifier: "detail", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
           if segue.identifier == "detail"{
               let des = segue.destination as! GuidersDetailViewController
            des.id = guidersArr[selectedIndex].id!
               
           }
       }
}
extension GuidersViewController{
func getGuiders()
    {
        SVProgressHUD.show()
        let color = #colorLiteral(red: 0.1607843137, green: 0.6823529412, blue: 0.2901960784, alpha: 1)
        SVProgressHUD.setForegroundColor(color)
        guidersArr.removeAll()
        let url = URL(string:"http://app.a2zhandymanuae.com/api/cities.php?city=" + id)
        
        URLSession.shared.dataTask(with: url!) { (data, response, err) in
            guard let data = data else{return}
            do{
                let json = try JSON(data:data)
                let dataa = json["data"]
                let guiderss = dataa["guiders"]
                  print(guiderss)
                for index in guiderss.arrayValue{
                self.guidersArr.append(guiders(json: index))
                }
                DispatchQueue.main.async {
                    
                    if self.guidersArr.count == 0{
                        self.empty.isHidden = false
                        self.tblVU.isHidden = true
                        
                    }
                    
                    self.tblVU.reloadData()
                    SVProgressHUD.dismiss()
                  
                }
                
            }
            catch{print("error")
                
            }
            }.resume()
    }
    
}
