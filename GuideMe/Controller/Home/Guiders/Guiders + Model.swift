//
//  City + Model.swift
//  GuideMe
//
//  Created by user on 22/02/2020.
//  Copyright © 2020 apple. All rights reserved.
//

import Foundation
import SwiftyJSON

struct guiders {
    var id : String?
    var name: String?
    var city : String?
    var rating : String?
    var image : String?
    init() {
        
    }
    
    init(json:JSON) {
        
        self.id = json["id"].stringValue
        self.name = json["name"].stringValue
        self.image = json["image"].stringValue
        self.city = json["city"].stringValue
        self.rating = json["rating"].stringValue
        
    }
    
}
