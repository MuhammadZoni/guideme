//
//  GuidersDetailViewController.swift
//  GuideMe
//
//  Created by user on 22/02/2020.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import AlamofireImage
import SVProgressHUD

class GuidersDetailViewController: UIViewController {

    var id = ""
    var titleArr = ["Name","Email","Phone Number","City","Experience","Car Model","Available Days"]
    var anss = [".",".",".",".",".",".","."]
    @IBOutlet weak var img: UIImageView!
    
    @IBOutlet weak var tblVu: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        getProfile()
        
    }
    
    @IBAction func tappedBackBtn(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func tapepdSendRequest(_ sender: Any) {
        makeReservations()
    }
}

extension GuidersDetailViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblVu.dequeueReusableCell(withIdentifier: "cell") as! GuiderDetailTableViewCell
        
        cell.titlee.text = titleArr[indexPath.row]
        cell.ans.text = anss[indexPath.row]
        
       // cell.layer.cornerRadius = 12.0
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            if segue.identifier == "detail"{
                let des = segue.destination as! YourRegistrationVC
                des.name = anss[0]
                des.mail = anss[1]
                des.phone = anss[2]
                des.id = id
                
            }
        }
    
    
}

extension GuidersDetailViewController{
    
    func getProfile(){
       
             //   self.anss.removeAll()
                let param:Parameters = ["guider":Int(id)]
                Alamofire.request("http://app.a2zhandymanuae.com/api/profile.php", method: .post, parameters: param).responseData { response in
                    switch response.result{
                    case.success(let value):
                     let json = JSON(value)
                     let visitor = json["visitor"]
                    
                     
                     DispatchQueue.main.async {
                        self.anss[0] = visitor["firstName"].stringValue + " " + visitor["lastName"].stringValue
                        
                        self.anss[1] = visitor["email"].stringValue
                        
                        self.anss[2] = visitor["phone"].stringValue
                        
                        self.anss[3] = visitor["city"].stringValue
                         self.anss[4] = visitor["experience"].stringValue
                         self.anss[5] = visitor["carModel"].stringValue
                        self.anss[6] = "24/7"
                        
                        if visitor["image"].stringValue == ""{
                            
                        }else{
                            self.img.af_setImage(withURL: URL.init(string: visitor["image"].stringValue)!)
                        }
                        
                        self.tblVu.reloadData()
                       
                     }

                        break
                    case.failure(let error):
                        print(error.localizedDescription)
                    }
                }
    }
        
    func makeReservations(){

        SVProgressHUD.show()
    
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        let result = formatter.string(from: date)

            if userOrGuider == 2{
                
                let param:Parameters = ["userId":userId,
                                        "guiderId":id,"place":anss[3],
                                        "date":result,"code":"wq992283"]
                Alamofire.request("http://app.a2zhandymanuae.com/api/reserve.php", method: .post, parameters: param).responseData { response in
                    switch response.result{
                    case.success(let value):
                        let json = JSON(value)
                        let data = json["status"].boolValue
                        
                        if data{
                          
                            self.performSegue(withIdentifier: "detail", sender: self)
                        } else {
                            AlertHelper.displayAlertMessage(messageToDisplay: "Invalid", sender: self)
                        }
                        SVProgressHUD.dismiss()
                        break

                    case.failure(let error):
                        print(error.localizedDescription)
                    }
                }
            
            }else{
                SVProgressHUD.dismiss()
                 AlertHelper.displayAlertMessage(messageToDisplay: "You are guider you cannot send request please create a user account ", sender: self)
        }
    }
}
