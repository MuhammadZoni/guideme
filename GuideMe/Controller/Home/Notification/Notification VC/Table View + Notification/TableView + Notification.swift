
//
//  TableView + Home.swift
//  GuideMe
//
//  Created by user on 26/02/2020.
//  Copyright © 2020 apple. All rights reserved.
//

import Foundation
import SwiftyJSON
import AlamofireImage
import Alamofire
import SVProgressHUD


extension NotificationViewController:UITableViewDelegate,UITableViewDataSource{
func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if userOrGuider == 1{
        return requestsArr.count
    }else{
        return visitorRequestsArr.count
    }
    
}
func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tblVu.dequeueReusableCell(withIdentifier: "cell") as! NotificationTableViewCell
   
    if userOrGuider == 1{
        if requestsArr.count != 0{
            cell.nameTitle.text = "Visitor Name"
            cell.name.text = requestsArr[indexPath.row].user
            cell.city.text = requestsArr[indexPath.row].place
            cell.stauts.text = requestsArr[indexPath.row].status
        }
        
    }else{
        if visitorRequestsArr.count != 0{
            cell.nameTitle.text = "Guider Name"
                   cell.name.text = visitorRequestsArr[indexPath.row].guiderName
                   cell.city.text = visitorRequestsArr[indexPath.row].place
                   cell.stauts.text = visitorRequestsArr[indexPath.row].status
        }
       
    }
    return cell
}
func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    selectedIndex = indexPath.row
    self.performSegue(withIdentifier: "detail", sender: self)
}

func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
    return 120
}


}
