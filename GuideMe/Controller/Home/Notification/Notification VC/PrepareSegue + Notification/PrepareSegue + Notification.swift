//
//  PrepareSegue + Notification.swift
//  GuideMe
//
//  Created by user on 26/02/2020.
//  Copyright © 2020 apple. All rights reserved.
//

import Foundation
import UIKit



extension NotificationViewController{
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
               if segue.identifier == "detail"{
                   let des = segue.destination as! DetailNotificationViewController
                
                if userOrGuider == 1{
                    
                    if requestsArr.count != 0{
                        des.requestID = requestsArr[selectedIndex].id!
                                          des.userId = ""
                                          des.mails = requestsArr[selectedIndex].userEmail!
                                          des.phones = requestsArr[selectedIndex].userPhone!
                                          des.names = requestsArr[selectedIndex].user!
                                          des.statuss = requestsArr[selectedIndex].status!
                        
                         des.citys = requestsArr[selectedIndex].place!
                        
                    }
                    
                    
                    
                    
                }else{
                    
                    if visitorRequestsArr.count != 0{
                        
                        c_id = visitorRequestsArr[selectedIndex].id!
                        g_Id = visitorRequestsArr[selectedIndex].guiderId!
                        
                        des.requestID = visitorRequestsArr[selectedIndex].id!
                                           des.userId = ""
                                           des.mails = visitorRequestsArr[selectedIndex].guiderEmail!
                                                             des.phones = visitorRequestsArr[selectedIndex].guiderPhone!
                                                             des.names = visitorRequestsArr[selectedIndex].guiderName!
                                                             des.statuss = visitorRequestsArr[selectedIndex].status!
                                           
                                            des.citys = visitorRequestsArr[selectedIndex].place!
                        
                        
                        
                    }
                    
                   
                }
                
                  
                   
               }
           
       }
}
