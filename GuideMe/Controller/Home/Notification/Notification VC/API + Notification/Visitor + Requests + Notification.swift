//
//  Guider + Request + Notification.swift
//  GuideMe
//
//  Created by user on 26/02/2020.
//  Copyright © 2020 apple. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD
import AlamofireImage
import Alamofire
import SwiftyJSON
   
extension NotificationViewController{
    func VisitorsRequestss(){
        SVProgressHUD.show()
        requestsArr.removeAll()
        let param:Parameters = ["user":userId]
    Alamofire.request("http://app.a2zhandymanuae.com/api/userReservations.php", method: .post, parameters: param).responseData { response in
        switch response.result{
                       case.success(let value):
                           let json = JSON(value)
                           let data = json["status"].boolValue
                           
                           if data{
                               let dataa = json["data"]
                            
                            for index in dataa.arrayValue{

                                self.visitorRequestsArr.append(visitorRequest(json: index))
                            }
                            DispatchQueue.main.async {
                                
                                if self.visitorRequestsArr.count == 0{
                                    self.tblVu.isHidden = true
                                      self.notFound.isHidden = false
                                }
                                
                                self.tblVu.reloadData()
                            }
                           } else {
                               AlertHelper.displayAlertMessage(messageToDisplay: "Empty", sender: self)
                            
                           }
                           SVProgressHUD.dismiss()
                           break

                       case.failure(let error):
                           print(error.localizedDescription)
                       }
                   }
       }
}


