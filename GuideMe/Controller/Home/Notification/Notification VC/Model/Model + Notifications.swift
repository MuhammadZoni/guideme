//
//  Model + Notifications.swift
//  GuideMe
//
//  Created by user on 23/02/2020.
//  Copyright © 2020 apple. All rights reserved.
//

import Foundation
import SwiftyJSON

struct requests {

    
    var id : String?
    var place :  String?
    var user : String?
    var status :  String?
    var userEmail : String?
    var userPhone :  String?
    
    
    
    init(){
        
        
    }
    
    init(json:JSON) {
        
        self.id = json["id"].stringValue
                self.place = json["place"].stringValue
                self.user = json["userName"].stringValue
                self.status = json["status"].stringValue
        self.userEmail = json["userEmail"].stringValue
                       self.userPhone = json["userPhone"].stringValue
        
        
    }
    
    
}



struct visitorRequest {
    
    var id : String?
    var place :  String?
    var guiderId : String?
    var guiderName :  String?
    var guiderEmail : String?
    var guiderPhone :  String?
    var status :  String?
    
    
    
    init(){
        
        
    }
    
    init(json:JSON) {
        
        self.id = json["id"].stringValue
                self.place = json["place"].stringValue
                self.guiderId = json["guiderId"].stringValue
                self.guiderName = json["guiderName"].stringValue
        self.guiderEmail = json["guiderEmail"].stringValue
                       self.guiderPhone = json["guiderPhone"].stringValue
         self.status = json["status"].stringValue
        
        
    }
    
    
}
