
//  RatingVC.swift
//  GuideMe
//
//  Created by apple on 2/23/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD

class RatingVC: UIViewController, UITextViewDelegate {
    var requestID = ""

    //MARK:- Outlets
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var oneStar: UIButton!
    @IBOutlet weak var twoStar: UIButton!
    @IBOutlet weak var threeStar: UIButton!
    @IBOutlet weak var fourStar: UIButton!
    @IBOutlet weak var fiveStar: UIButton!
    @IBOutlet var buttons: [UIButton]!
    
    //MARK:- Variables
    var rating:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        textView.layer.cornerRadius = 8
        textView.layer.borderWidth = 1
        textView.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        textViewPlaceholder()
    }
    
    @IBAction func tappedBackBtn(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    @IBAction func ratingClicked(_ sender: UIButton) {
        if sender.tag == 1{
            rating = "1"
            oneStar.setImage(UIImage(named: "star"), for: .normal)
            twoStar.setImage(UIImage(named: "star (1)"), for: .normal)
            threeStar.setImage(UIImage(named: "star (1)"), for: .normal)
            fourStar.setImage(UIImage(named: "star (1)"), for: .normal)
            fiveStar.setImage(UIImage(named: "star (1)"), for: .normal)
            
        } else if sender.tag == 2{
            rating = "2"
            oneStar.setImage(UIImage(named: "star"), for: .normal)
            twoStar.setImage(UIImage(named: "star"), for: .normal)
            threeStar.setImage(UIImage(named: "star (1)"), for: .normal)
            fourStar.setImage(UIImage(named: "star (1)"), for: .normal)
            fiveStar.setImage(UIImage(named: "star (1)"), for: .normal)
        } else if sender.tag == 3{
            rating = "3"
            oneStar.setImage(UIImage(named: "star"), for: .normal)
            twoStar.setImage(UIImage(named: "star"), for: .normal)
            threeStar.setImage(UIImage(named: "star"), for: .normal)
            fourStar.setImage(UIImage(named: "star (1)"), for: .normal)
            fiveStar.setImage(UIImage(named: "star (1)"), for: .normal)
        } else if sender.tag == 4{
            rating = "4"
            oneStar.setImage(UIImage(named: "star"), for: .normal)
            twoStar.setImage(UIImage(named: "star"), for: .normal)
            threeStar.setImage(UIImage(named: "star"), for: .normal)
            fourStar.setImage(UIImage(named: "star"), for: .normal)
            fiveStar.setImage(UIImage(named: "star (1)"), for: .normal)
        } else if sender.tag == 5{
            rating = "5"
            oneStar.setImage(UIImage(named: "star"), for: .normal)
            twoStar.setImage(UIImage(named: "star"), for: .normal)
            threeStar.setImage(UIImage(named: "star"), for: .normal)
            fourStar.setImage(UIImage(named: "star"), for: .normal)
            fiveStar.setImage(UIImage(named: "star"), for: .normal)
        }
    }
    
    @IBAction func postBtnClicked(_ sender: UIButton) {
        
        if textView.text == "Enter Review" || rating == ""{
            AlertHelper.displayAlertMessage(messageToDisplay: "Choose rating & Enter review for the Guider", sender: self)
        } else {
            
            
//            let id = UserDefaults.standard.object(forKey: "reqId")
//
//                            var reqID = ""
//
//                            if let token = id as? String{
//                                reqID = token
//
//                            }
            
            SVProgressHUD.show()
            let param:Parameters = ["userId":"\(userId)",
                "guiderId":"\(g_Id)",
                "rating":rating,
                "comments":textView.text!
            ]
            Alamofire.request("http://app.a2zhandymanuae.com/api/reviewGuider.php", method: .post, parameters: param).responseData { response in
                switch response.result{
                case.success(let value):
                    let json = JSON(value)
                    let status = json["status"].boolValue
                    if status{
                        SVProgressHUD.dismiss()
                        self.alert()
                    }
                case.failure(let error):
                    print(error.localizedDescription)
                }
            }
        }
        
    }
    //Adding Placeholder to the UITextview
    func textViewPlaceholder(){
        textView.text = "Enter Review"
        textView.textColor = UIColor.white
        textView.returnKeyType = .done
        textView.delegate = self
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Enter Review"
        {
            textView.text = ""
            textView.textColor = UIColor.white
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
    
}

extension RatingVC{
    func alert() {
        
        let alerts = UIAlertController.init(title: "eview added successfully, Thanks for your review" , message: "", preferredStyle: .alert)
               alerts.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (uiaction:UIAlertAction) in
                 let home = self.storyboard?.instantiateViewController(withIdentifier: "home") as! tabbarViewController
                  self.present(home, animated: false, completion: nil)
               }))
               self.present(alerts, animated: true, completion: nil)
        
    }
}
