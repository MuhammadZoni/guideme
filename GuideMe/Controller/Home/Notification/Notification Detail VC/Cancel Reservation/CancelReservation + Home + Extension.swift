//
//  Cancel Reservation.swift
//  GuideMe
//
//  Created by user on 26/02/2020.
//  Copyright © 2020 apple. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD
import SwiftyJSON
import Alamofire
import AlamofireImage

extension DetailNotificationViewController{
    func cannelReservations(){
           SVProgressHUD.show()
//        let id = UserDefaults.standard.object(forKey: "reqId")
//                 var reqID = ""
//                 if let token = id as? String{
//                     reqID = token
//                 }
        
        let param:Parameters = ["id":c_id]
  

        Alamofire.request("http://app.a2zhandymanuae.com/api/cancelReservation.php", method: .post, parameters: param).responseData { response in
                       switch response.result{
                       case.success(let value):
                           let json = JSON(value)
                           let data = json["status"].boolValue
                           
                           if data{
                            
                               let home = self.storyboard?.instantiateViewController(withIdentifier: "home") as! tabbarViewController
                               self.present(home, animated: false, completion: nil)
                           } else {
                               AlertHelper.displayAlertMessage(messageToDisplay: "Invalid", sender: self)
                           }
                           SVProgressHUD.dismiss()
                           break

                       case.failure(let error):
                           print(error.localizedDescription)
                       }
                   }
       }
}
