//
//  DetailNotificationViewController.swift
//  GuideMe
//
//  Created by user on 23/02/2020.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit
import SwiftyJSON
import SVProgressHUD
import Alamofire

class DetailNotificationViewController: UIViewController {

    
    //var guiderId = ""
    var userId = ""
    var requestID = ""
    var names = ""
    var statuss = ""
    var phones = ""
    var mails = ""
    var citys = ""
    
    @IBOutlet weak var cancelReservationbtn: UIButton!
    @IBOutlet weak var rattingBtn: UIButton!
    @IBOutlet weak var citytitle: UILabel!
    @IBOutlet weak var phoneTitle: UILabel!
    @IBOutlet weak var emailTitle: UILabel!
    @IBOutlet weak var nameTitle: UILabel!
    
    @IBOutlet weak var city: UILabel!
    @IBOutlet weak var mail: UILabel!
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var name: UILabel!
   
    @IBOutlet weak var okaybtn: UIButton!
    @IBOutlet weak var acceptBtn: UIButton!
    @IBOutlet weak var declineBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if userOrGuider == 1{
            nameTitle.text = "Guider Name"
            emailTitle.text = "Guider Mail"
            phoneTitle.text = "Guider Phone"
            citytitle.text = "Guider City"
        }else{
            nameTitle.text = "User Name"
            emailTitle.text = "User Mail"
            phoneTitle.text = "User Phone"
            citytitle.text = "User City"
        }
        name.text = names
        city.text = citys
        status.text = statuss
        phone.text = phones
        mail.text = mails
        
    }
    
    @IBAction func tappedRattingAndReview(_ sender: Any) {
        self.performSegue(withIdentifier: "rating", sender: self)
    }
    
    @IBAction func tappedCancelReservation(_ sender: Any) {
        cannelReservations()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        if userOrGuider == 2{
            
            rattingBtn.isHidden = false
            cancelReservationbtn.isHidden = false
            acceptBtn.isHidden = true
            declineBtn.isHidden = true
            okaybtn.isHidden = true
        }else{
            
            rattingBtn.isHidden = true
            cancelReservationbtn.isHidden = true
            
            
            if statuss == "cancelled"{
                       acceptBtn.isHidden = true
                       declineBtn.isHidden = true
                       okaybtn.isHidden = false
                       
                   }else if statuss == "pending"{
                   acceptBtn.isHidden = false
                   declineBtn.isHidden = false
                   okaybtn.isHidden = true
                   }else{
                       acceptBtn.isHidden = true
                       declineBtn.isHidden = true
                       okaybtn.isHidden = false
                   }
        }
        
       
    }
    
    @IBAction func tappedOkayBtn(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func tappedDeclineBtn(_ sender: Any) {
        decline()
    }
    
    @IBAction func tappedAcceotBtn(_ sender: Any) {
        accepts()
    }
    
}




extension DetailNotificationViewController{
    func accepts(){

           SVProgressHUD.show()
       
      //  requestsArr.removeAll()

        
                   let param:Parameters = ["id":requestID]
                   Alamofire.request("http://app.a2zhandymanuae.com/api/acceptReservation.php", method: .post, parameters: param).responseData { response in
                       switch response.result{
                       case.success(let value):
                           let json = JSON(value)
                           let data = json["status"].boolValue
                           
                           if data{
                           
                            
                            let home = self.storyboard?.instantiateViewController(withIdentifier: "home") as! tabbarViewController
                            self.present(home, animated: false, completion: nil)
                            
                             AlertHelper.displayAlertMessage(messageToDisplay: "Reservation Accepted Successfully Please Contact Him (Visitor) Through His Phone Number or Email", sender: self)
                            
                           } else {
                               AlertHelper.displayAlertMessage(messageToDisplay: "Empty", sender: self)
                            
                           }
                           SVProgressHUD.dismiss()
                           break

                       case.failure(let error):
                           print(error.localizedDescription)
                       }
                   }
               
               
       }
    
    
    
    func decline(){

              SVProgressHUD.show()
          
         //  requestsArr.removeAll()

           
                      let param:Parameters = ["id":requestID]
                      Alamofire.request("http://app.a2zhandymanuae.com/api/cancelReservation.php", method: .post, parameters: param).responseData { response in
                          switch response.result{
                          case.success(let value):
                              let json = JSON(value)
                              let data = json["status"].boolValue
                              
                              if data{
                              
                               
                               let home = self.storyboard?.instantiateViewController(withIdentifier: "home") as! tabbarViewController
                               self.present(home, animated: false, completion: nil)
                               
                                AlertHelper.displayAlertMessage(messageToDisplay: "Reservation Decline Successfully", sender: self)
                               
                              } else {
                                  AlertHelper.displayAlertMessage(messageToDisplay: "Empty", sender: self)
                               
                              }
                              SVProgressHUD.dismiss()
                              break

                          case.failure(let error):
                              print(error.localizedDescription)
                          }
                      }
                  
                  
          }
}
