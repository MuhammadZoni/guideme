//
//  NotificationRepeater.swift
//  GuideMe
//
//  Created by user on 26/02/2020.
//  Copyright © 2020 apple. All rights reserved.
//

import Foundation
import SVProgressHUD
import SwiftyJSON
import UIKit
import Alamofire
import AlamofireImage
import UserNotifications


extension HomeVC{
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
           completionHandler([.alert , .badge , .sound])
       }
       func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
          
        let home = self.storyboard?.instantiateViewController(withIdentifier: "home") as! tabbarViewController
                                    
        self.present(home, animated: false, completion: nil)
        
        
       }
}
extension HomeVC{
    func requestss(){
        print("zoni")
        self.requestsArr.removeAll()
        let param:Parameters = ["guider":guiderId]
        Alamofire.request("http://app.a2zhandymanuae.com/api/reservations.php", method: .post, parameters: param).responseData { response in
        switch response.result{
                       case.success(let value):
                           let json = JSON(value)
                           let data = json["status"].boolValue
                           if data{
                               let dataa = json["data"]
                            for index in dataa.arrayValue{
                                self.requestsArr.append(requests(json: index))
                            }
                            if !self.checkis{
                                self.checkis = true
                                self.re = self.requestsArr
                            }
                            if self.requestsArr.count > self.re.count{
                                 self.re = self.requestsArr
                                self.notificationContentPart()
                                  self.re = self.requestsArr
                            }
                            DispatchQueue.main.async {
                                
                            }
                           }else {
                               AlertHelper.displayAlertMessage(messageToDisplay: "Empty", sender: self)
                            
                           }
                           SVProgressHUD.dismiss()
                           break
                       case.failure(let error):
                           print(error.localizedDescription)
                       }
                   }
       }
}

extension HomeVC{
   @objc func updateCounting(){requestss()}
    func notificationContentPart(){
        //Mark:- Content Part
                                             let content = UNMutableNotificationContent()
                                             content.categoryIdentifier = ""
                                             content.title = "Guide Me"
                                             content.body = "You Have New Reservation"
                                             content.badge = 1
                                             content.sound = UNNotificationSound.default
        //                                     let url = Bundle.main.url(forResource: "2", withExtension: "png")
        //                                     let attachment = try! UNNotificationAttachment(identifier: "image", url: url!, options: [:])
        //                                     content.attachments = [attachment]
                                             let triger = UNTimeIntervalNotificationTrigger.init(timeInterval: 2, repeats: false)
                                             let identifier = "Main Identifier"
                                             let request = UNNotificationRequest.init(identifier: identifier, content: content, trigger: triger)
                                             notification.add(request) { (error) in
                                                 print(error?.localizedDescription as Any)
                                             }
    }
}
