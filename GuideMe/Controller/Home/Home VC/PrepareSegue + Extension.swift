//
//  PrepareSegue + Extension.swift
//  GuideMe
//
//  Created by user on 26/02/2020.
//  Copyright © 2020 apple. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import AlamofireImage
import Alamofire
import SVProgressHUD

extension HomeVC{

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
           if segue.identifier == "guiders"{
               let des = segue.destination as! GuidersViewController
               
               if searching{
               des.id = currentCityArr[selectedIndex].id!
               des.cityNames = currentCityArr[selectedIndex].name!
               }else{
                   des.id = citiesArr[selectedIndex].id!
                   des.cityNames = citiesArr[selectedIndex].name!
               }
               
               
           }
       }

}

