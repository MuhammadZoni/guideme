//
//  City + Model.swift
//  GuideMe
//
//  Created by user on 22/02/2020.
//  Copyright © 2020 apple. All rights reserved.
//

import Foundation
import SwiftyJSON
import UIKit
struct cities {
    var id : String?
    var name: String?
    var img : String?
    
    init() {
        
    }
    
    init(json:JSON) {
        self.id = json["id"].stringValue
        self.name = json["name"].stringValue
        self.img = json["image"].stringValue
    }
}
