//
//  CollectionVu + Home + Extension.swift
//  GuideMe
//
//  Created by user on 26/02/2020.
//  Copyright © 2020 apple. All rights reserved.
//
import UIKit
import Foundation
import SVProgressHUD
import SwiftyJSON
import AlamofireImage
import Alamofire

extension HomeVC:UICollectionViewDelegate,UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        if searching{
            return currentCityArr.count
        } else {
             return citiesArr.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        let cell = collectionVu.dequeueReusableCell(withReuseIdentifier: "homeCell", for: indexPath) as! homeCollectionViewCell
        
        if searching{
            cell.layer.cornerRadius = 8.0
            cell.name.text = currentCityArr[indexPath.row].name
            cell.img.af_setImage(withURL: URL.init(string: currentCityArr[indexPath.row].img!)!)
        } else {
            cell.layer.cornerRadius = 8.0
            cell.name.text = citiesArr[indexPath.row].name
            cell.img.af_setImage(withURL: URL.init(string: citiesArr[indexPath.row].img!)!)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        selectedIndex = indexPath.row
        performSegue(withIdentifier: "guiders", sender: self)
    }
    
}

extension HomeVC: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize{
        let collectionViewSize = collectionView.bounds.size.width
        if indexPath.row == 0 || indexPath.row == 3 || indexPath.row == 6 || indexPath.row == 9 || indexPath.row == 12 {
            return CGSize(width: collectionViewSize-10, height: collectionViewSize/2-10)
        }
        return CGSize(width: collectionViewSize/2-5, height: collectionViewSize/2-5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        return 10
    }
}



     
