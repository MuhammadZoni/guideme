//
//  City + API.swift
//  GuideMe
//
//  Created by user on 26/02/2020.
//  Copyright © 2020 apple. All rights reserved.
//

import Foundation
import SwiftyJSON
import UIKit
import AlamofireImage
import Alamofire
import SVProgressHUD

extension HomeVC{
func getCity()
    {
        SVProgressHUD.show()
        let color = #colorLiteral(red: 0.1607843137, green: 0.6823529412, blue: 0.2901960784, alpha: 1)
        SVProgressHUD.setForegroundColor(color)
        citiesArr.removeAll()
        let url = URL(string:"http://app.a2zhandymanuae.com/api/cities.php")
        
        URLSession.shared.dataTask(with: url!) { (data, response, err) in
            guard let data = data else{return}
            do{
                let json = try JSON(data:data)
                let dataa = json["data"]
                                  
                for index in dataa.arrayValue{
                self.citiesArr.append(cities(json: index))
                }
                DispatchQueue.main.async {
                    self.collectionVu.reloadData()
                    SVProgressHUD.dismiss()
                }
            }
            catch{print("error")
            }
            }.resume()
    }
    
}


