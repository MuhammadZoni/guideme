//
//  Search + City + Home + Extension.swift
//  GuideMe
//
//  Created by user on 26/02/2020.
//  Copyright © 2020 apple. All rights reserved.
//
import UIKit
import Foundation
import SVProgressHUD
import SwiftyJSON
import AlamofireImage
import Alamofire

extension HomeVC: UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            currentCityArr = citiesArr
            collectionVu.reloadData()
            return
        }
        currentCityArr = citiesArr.filter({ filterCities -> Bool in
            //guard let text = searchBar.text else { return false }
            return filterCities.name!.lowercased().contains(searchText.lowercased())
        })
        searching = true
        collectionVu.reloadData()
        //print(searchArray)
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        //searchstate = false
        self.searchBar.endEditing(true)
        searchBar.resignFirstResponder()
        
    }
}

