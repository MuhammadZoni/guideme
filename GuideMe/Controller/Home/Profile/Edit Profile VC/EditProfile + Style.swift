//
//  EditProfile + Style.swift
//  GuideMe
//
//  Created by user on 22/02/2020.
//  Copyright © 2020 apple. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SVProgressHUD
import SwiftyJSON

extension EditProfileVC{
func style(){
    
    self.txtFirstName.text = jsonObject!["firstName"].stringValue
    self.txtLastName.text = jsonObject!["lastName"].stringValue
    self.txtEmail.text = jsonObject!["email"].stringValue
    self.txtPhone.text = jsonObject!["phone"].stringValue
    self.txtCountry.text = jsonObject!["country"].stringValue
    self.txtDob.text = jsonObject!["dob"].stringValue
                           
    if jsonObject!["image"].stringValue == ""{
                               
                           }else{
        self.profileImg.af_setImage(withURL: URL.init(string: jsonObject!["image"].stringValue)!)
                           }
    
    
           txtFirstName.layer.cornerRadius = 8.0
           txtLastName.layer.cornerRadius = 8.0
           txtEmail.layer.cornerRadius = 8.0
           txtCountry.layer.cornerRadius = 8.0
           txtPhone.layer.cornerRadius = 8.0
           txtDob.layer.cornerRadius = 8.0
           
    
           txtFirstName.layer.borderWidth = 1
           txtFirstName.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
           txtLastName.layer.borderWidth = 1
           txtLastName.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
           txtEmail.layer.borderWidth = 1
           txtEmail.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
           txtPhone.layer.borderWidth = 1
           txtPhone.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
           txtCountry.layer.borderWidth = 1
          txtCountry.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
         txtDob.layer.borderWidth = 1
         txtDob.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
         
       
}
}

