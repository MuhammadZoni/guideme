//
//  GuiderEditViewController.swift
//  GuideMe
//
//  Created by user on 23/02/2020.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import AlamofireImage
import SVProgressHUD


class GuiderEditViewController: UIViewController {
    
    //Mark :- Variables
    var isImageSelected:Bool = false
    //Mark :- Arrays
    var jsonObject : JSON?

    //Mark :- outlets
      @IBOutlet weak var profileImg: UIImageView!
      @IBOutlet weak var txtFirstName: UITextField!
      @IBOutlet weak var txtLastName: UITextField!
      @IBOutlet weak var txtEmail: UITextField!
      @IBOutlet weak var txtPhone: UITextField!
      @IBOutlet weak var txtCountry: UITextField!
      @IBOutlet weak var txtDob: UITextField!
    
    @IBOutlet weak var txtexpe: UITextField!
    @IBOutlet weak var txtnid: UITextField!
    @IBOutlet weak var txtcarmodel: UITextField!
    @IBOutlet weak var txtcarnoplate: UITextField!
    @IBOutlet weak var txtcartype: UITextField!
    @IBOutlet weak var txtcharges: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        style()
    }
    @IBAction func tappedChangePorfileImg(_ sender: Any) {
        openGallary()
    }
    @IBAction func tappedBackBtn(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func updateBtnClick(_ sender: UIButton) {
        if isImageSelected{
            
            saveSettings(userID: "\(userId)", image: profileImg.image!) { (isSuccessfull, err) in
                if isSuccessfull{
                    print("Yahoooo! I did it")
                    SVProgressHUD.dismiss()
                    self.updateUserData()
                   
                } else{
                    print(err)
                }
            }
        } else {
            updateUserData()
        }
    }

    
    func updateUserData(){
        SVProgressHUD.show()
        let param:Parameters = [
            "perHour":txtcharges.text!,
            "carPlate":txtcarnoplate.text!,
            "carModel":txtcarmodel.text!,
            "carType":txtcartype.text!,
            "experience":".",
            "city":txtCountry.text!,
            "availability":"mon,tues,wed",
            "password":password,
            "firstName":txtFirstName.text!,
            "lastName":txtLastName.text!,
            "nid":txtnid.text!,
            "email":txtEmail.text!,
            "phone":txtPhone.text!,
            "dob":txtDob.text!,
            "id":guiderId
        ]
        Alamofire.request("http://app.a2zhandymanuae.com/api/updateGuiderProfile.php", method: .post, parameters: param).responseData { response in
            switch response.result{
            case.success(let value):
                let json = JSON(value)
                let status = json["status"].boolValue
                if status{
                    let home = self.storyboard?.instantiateViewController(withIdentifier: "home") as! tabbarViewController
                                                                                self.present(home, animated: false, completion: nil)
                    AlertHelper.displayAlertMessage(messageToDisplay: "You profile updated Successfully", sender: self)
                //    self.getProfileData()
                   
                }
                SVProgressHUD.dismiss()
            case.failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}

extension GuiderEditViewController: UIImagePickerControllerDelegate , UINavigationControllerDelegate
{
    func openGallary()
    {
        let imagePicketController = UIImagePickerController()
        imagePicketController.delegate = self
        
        let alerts = UIAlertController.init(title: "Select an option", message: "", preferredStyle: .actionSheet)
        alerts.addAction(UIAlertAction(title: "Camara", style: .default, handler: { (action:UIAlertAction) in
            if UIImagePickerController.isSourceTypeAvailable(.camera)
            {
                imagePicketController.sourceType = .camera
                self.present(imagePicketController, animated: true, completion: nil)
            } else {
                print("Camera not available")
            }
        }))
        alerts.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action:UIAlertAction) in
            imagePicketController.sourceType = .photoLibrary
            self.present(imagePicketController, animated: true, completion: nil)
            
        }))
        alerts.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alerts, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        picker.dismiss(animated: true, completion: nil)
        
        let imageSelected = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        profileImg.image = imageSelected
        isImageSelected = true
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}

extension GuiderEditViewController{
    func saveSettings(userID :String,image: UIImage, completion:@escaping (_ isSuccesfull :Bool, _ errorMessage :String?) ->()){
        SVProgressHUD.show()
        let user_id = userID //UserDefaults.standard.value(forKey: KeyValues.userIdkey) else {return}
        let imgData = UIImage.jpegData(image)//(image, 1.0)
        let urlReq = "http://app.a2zhandymanuae.com/api/addGuiderImage.php"
        
        let parameters = ["id": "\(guiderId)",]
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data"
        ]
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(image.pngData() ?? Data(), withName: "image",fileName: "Myfile.png", mimeType: "image/png")
            
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        },
                         to:"\(urlReq)")
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                    
                })
                
                upload.responseJSON { response in
                    print(response.result.value)
                    self.profileImg.image = image
                    completion(true, "Setting updated Successfully!")
                }
            case .failure(let encodingError):
                completion(false, "You failed to update your setting")
                print(encodingError)
            }
        }
    }

}
