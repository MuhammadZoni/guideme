//
//  GuiderEdit + Style.swift
//  GuideMe
//
//  Created by user on 23/02/2020.
//  Copyright © 2020 apple. All rights reserved.
//


import SVProgressHUD
import SwiftyJSON
import Alamofire
import UIKit
import AlamofireImage

extension GuiderEditViewController{
    
func style(){
    
    self.txtexpe.text = String(jsonObject!["experience"].intValue)
    self.txtnid.text = String(jsonObject!["nid"].stringValue)
    self.txtcarmodel.text = jsonObject!["carModel"].stringValue
    self.txtcarnoplate.text = jsonObject!["carPlate"].stringValue
    self.txtcartype.text = jsonObject!["carType"].stringValue
    self.txtcharges.text = String(jsonObject!["perHour"].intValue)
    
    self.txtFirstName.text = jsonObject!["firstName"].stringValue
    self.txtLastName.text = jsonObject!["lastName"].stringValue
    self.txtEmail.text = jsonObject!["email"].stringValue
    self.txtPhone.text = jsonObject!["phone"].stringValue
    self.txtCountry.text = jsonObject!["city"].stringValue
    self.txtDob.text = jsonObject!["dob"].stringValue
                           
    if jsonObject!["image"].stringValue == ""{
                               
                           }else{
        self.profileImg.af_setImage(withURL: URL.init(string: jsonObject!["image"].stringValue)!)
                           
    }
    
              txtexpe.layer.cornerRadius = 8.0
              txtnid.layer.cornerRadius = 8.0
              txtcarmodel.layer.cornerRadius = 8.0
              txtcarnoplate.layer.cornerRadius = 8.0
              txtcartype.layer.cornerRadius = 8.0
              txtcharges.layer.cornerRadius = 8.0
    
           txtFirstName.layer.cornerRadius = 8.0
           txtLastName.layer.cornerRadius = 8.0
           txtEmail.layer.cornerRadius = 8.0
           txtCountry.layer.cornerRadius = 8.0
           txtPhone.layer.cornerRadius = 8.0
           txtDob.layer.cornerRadius = 8.0
           

    
            txtexpe.layer.borderWidth = 1
            txtexpe.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            txtnid.layer.borderWidth = 1
            txtnid.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            txtcarmodel.layer.borderWidth = 1
            txtcarmodel.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            txtcarnoplate.layer.borderWidth = 1
            txtcarnoplate.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            txtcartype.layer.borderWidth = 1
            txtcartype.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            txtcharges.layer.borderWidth = 1
            txtcharges.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    
    
    
            txtFirstName.layer.borderWidth = 1
      txtFirstName.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
      txtLastName.layer.borderWidth = 1
      txtLastName.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
      txtEmail.layer.borderWidth = 1
      txtEmail.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
      txtPhone.layer.borderWidth = 1
      txtPhone.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
      txtCountry.layer.borderWidth = 1
     txtCountry.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    txtDob.layer.borderWidth = 1
    txtDob.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
         
       
}
}

