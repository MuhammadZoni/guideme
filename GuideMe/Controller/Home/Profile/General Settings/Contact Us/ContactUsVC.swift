//
//  ContactUsVC.swift
//  GuideMe
//
//  Created by apple on 2/20/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit
import SVProgressHUD
import AlamofireImage
import Alamofire
import SwiftyJSON

class ContactUsVC: UIViewController {

    //Mark :- Variables
       
    //Mark :- Arrays
       
    //Mark :- outlets
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtMsg: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        style()

    }
    
    @IBAction func tappedSendBtn(_ sender: Any) {
        
    }
    @IBAction func tappedBackBtn(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
}
extension ContactUsVC:UITextViewDelegate{
    func style(){
        txtName.layer.cornerRadius = 8.0
               txtEmail.layer.cornerRadius = 8.0
               txtMsg.layer.cornerRadius = 8.0
               txtName.layer.borderWidth = 1
               txtName.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
               txtEmail.layer.borderWidth = 1
               txtEmail.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
               txtMsg.layer.borderWidth = 1
               txtMsg.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
               self.txtMsg.text = "  Enter Message"
               self.txtMsg.textColor = UIColor.white
               txtMsg.returnKeyType = .done
               self.txtMsg.delegate = self
              
               txtName.attributedPlaceholder = NSAttributedString(string: "Enter Your Name",
                                                               attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)])
               txtEmail.attributedPlaceholder = NSAttributedString(string: "Enter Your Email",
               attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)])
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if txtMsg.text == "  Enter Message"{
            txtMsg.text = ""
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n"{
            txtMsg.resignFirstResponder()
        }
        return true
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if txtMsg.text == ""{
            txtMsg.text = "  Enter Message"
        }
    }
    func sendMessage(){

           SVProgressHUD.show()
       
           let date = Date()
           let formatter = DateFormatter()
           formatter.dateFormat = "dd.MM.yyyy"
           let result = formatter.string(from: date)

               if userOrGuider == 2{
                   
                let param:Parameters = ["name":txtName.text!,
                                        "email":txtEmail.text!,
                    "message":txtMsg.text!]
                   Alamofire.request("http://app.a2zhandymanuae.com/api/contact.php", method: .post, parameters: param).responseData { response in
                       switch response.result{
                       case.success(let value):
                           let json = JSON(value)
                           let data = json["status"].boolValue
                           
                           if data{
                            self.alert()
                           } else {
                               AlertHelper.displayAlertMessage(messageToDisplay: "Invalid", sender: self)
                           }
                           SVProgressHUD.dismiss()
                           break

                       case.failure(let error):
                           print(error.localizedDescription)
                       }
                   }
               
               }else{
                   SVProgressHUD.dismiss()
                    AlertHelper.displayAlertMessage(messageToDisplay: "cannot send  ", sender: self)
           }
       }
}



extension ContactUsVC{
    func alert() {
        
        let alerts = UIAlertController.init(title: "Send Successfully" , message: "", preferredStyle: .alert)
               alerts.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (uiaction:UIAlertAction) in
                 let home = self.storyboard?.instantiateViewController(withIdentifier: "home") as! tabbarViewController
                  self.present(home, animated: false, completion: nil)
               }))
               self.present(alerts, animated: true, completion: nil)
        
    }
}
