//
//  GeneralSettingVC.swift
//  GuideMe
//
//  Created by apple on 2/20/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit
import SwiftyJSON
import SVProgressHUD
import AlamofireImage
import Alamofire

class GeneralSettingVC: UIViewController {

    
    var jsonObject : JSON?
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    @IBAction func tappedBackBtn(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    @IBAction func tappedEditPorfileBtn(_ sender: Any) {
        if userOrGuider == 1{
              self.performSegue(withIdentifier: "editProfilea", sender: self)
        }else{
              self.performSegue(withIdentifier: "editProfile", sender: self)
        }
      
    }
    @IBAction func tappedLanguageSettingBtn(_ sender: Any) {
       
        
    }
    @IBAction func tappedTermConditionsBtn(_ sender: Any) {
        self.performSegue(withIdentifier: "terms", sender: self)
    }
    @IBAction func tappedPrivacyPolicy(_ sender: Any) {
        self.performSegue(withIdentifier: "aboutUs", sender: self)
    }
    @IBAction func tappedRateThisApp(_ sender: Any) {
       if let url = Bundle.main.url(forResource: "pp", withExtension: "pdf") {
           let webView = UIWebView(frame: self.view.frame)
           let urlRequest = URLRequest(url: url)
           webView.loadRequest(urlRequest as URLRequest)
           
           let pdfVC = UIViewController()
           pdfVC.view.addSubview(webView)
           pdfVC.title = "pp"
        self.present(pdfVC, animated: false, completion: nil)
          // self.navigationController?.pushViewController(pdfVC, animated: true)
           
           
       }
    }
    @IBAction func tappedContactUsBtn(_ sender: Any) {
        self.performSegue(withIdentifier: "contactUs", sender: self)
    }
 
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
              if segue.identifier == "editProfile"{
                  let des = segue.destination as! EditProfileVC
                  des.jsonObject = jsonObject
          }else if segue.identifier == "editProfilea"{
                           let des = segue.destination as! GuiderEditViewController
                           des.jsonObject = jsonObject
                           
                       }
                   }
}
