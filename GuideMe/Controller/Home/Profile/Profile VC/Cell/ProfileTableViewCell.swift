//
//  ProfileTableViewCell.swift
//  GuideMe
//
//  Created by user on 21/02/2020.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var icon: UIImageView!
     @IBOutlet weak var headingName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
