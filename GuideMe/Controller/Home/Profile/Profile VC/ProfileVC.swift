//
//  ProfileVC.swift
//  GuideMe
//
//  Created by apple on 2/20/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit
import AlamofireImage
import SVProgressHUD
import SwiftyJSON
import Alamofire

class ProfileVC: UIViewController {
        //Mark :- Variables
        
        //Mark :- Arrays
    var JSONObj : JSON?
        var headingArr = ["Settings","Logout"]
        var headingIcon = [#imageLiteral(resourceName: "settings"),#imageLiteral(resourceName: "logout")]
        //Mark :- outlets
        @IBOutlet weak var imgBgVu: UIView!
        @IBOutlet weak var imgVu: UIImageView!
        @IBOutlet weak var upprImgVU: UIView!
        @IBOutlet weak var editIcon: UIImageView!
        @IBOutlet weak var editProfileLbl: UILabel!
        @IBOutlet weak var tblVu: UITableView!
        @IBOutlet weak var name: UILabel!
        
        @IBOutlet weak var heading: UILabel!
        @IBOutlet weak var backBtn: UIButton!
        
        override func viewDidLoad() {
            super.viewDidLoad()
            style()
            
        
           
        }
        
        override func viewWillAppear(_ animated: Bool) {
            getProfile()
        }
        
        @objc func tapGestureAction(){
            if userOrGuider == 1{
                 performSegue(withIdentifier: "editProfilea", sender: self)
            }else{
                 performSegue(withIdentifier: "editProfile", sender: self)
            }
           
        }
        
        @IBAction func tappedBackBtn(_ sender: Any) {
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    extension ProfileVC : UITableViewDelegate , UITableViewDataSource
    {
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return headingArr.count
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tblVu.dequeueReusableCell(withIdentifier: "cell") as! ProfileTableViewCell

            cell.headingName.text = headingArr[indexPath.row]
            cell.icon.image = headingIcon[indexPath.row]
            
            return cell
        }
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 50
        }
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

            switch indexPath.row {
            case 0:
                performSegue(withIdentifier: "setting", sender: self)
                break
            case 1:
                let LoginScreen = self.storyboard?.instantiateViewController(withIdentifier: "login") as! WellComeHomeViewController
                self.present(LoginScreen, animated: false, completion: nil)
                break
            default:
                print("")
            }
            
        }
}
extension ProfileVC{
    func style(){
        tblVu.tableFooterView = UIView()
                   imgBgVu.layer.cornerRadius = imgBgVu.frame.size.height/2
                   imgVu.layer.cornerRadius = imgVu.frame.size.height/2
                   upprImgVU.layer.cornerRadius = upprImgVU.frame.size.height/2
                   imgBgVu.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                   imgBgVu.layer.borderWidth = 2.0
                   
                   let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureAction))
                   upprImgVU.addGestureRecognizer(tapGesture)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
           if segue.identifier == "editProfile"{
               let des = segue.destination as! EditProfileVC
               des.jsonObject = JSONObj
               
           }else if segue.identifier == "setting"{
           let des = segue.destination as! GeneralSettingVC
           des.jsonObject = JSONObj
        }else if segue.identifier == "editProfilea"{
            let des = segue.destination as! GuiderEditViewController
            des.jsonObject = JSONObj
            
        }
       }
}


extension ProfileVC{
    
    func getProfile(){
        SVProgressHUD.show()
        var params = ""
        var paramsID = 0
            if userOrGuider == 1{
                params = "guider"
                paramsID = guiderId
           }else if userOrGuider == 2{
                params = "visitor"
                paramsID = userId
           }
                
                let param:Parameters = [params:paramsID]
                Alamofire.request("http://app.a2zhandymanuae.com/api/profile.php", method: .post, parameters: param).responseData { response in
                    switch response.result{
                    case.success(let value):
                     let json = JSON(value)
                     let visitor = json["visitor"]
                     self.JSONObj = visitor
                     DispatchQueue.main.async {
                        self.name.text = visitor["firstName"].stringValue + " " + visitor["lastName"].stringValue
                        
                        if visitor["image"].stringValue == ""{
                            
                        }else{
                             self.imgVu.af_setImage(withURL: URL.init(string: visitor["image"].stringValue)!)
                        }
                        
                       
                     }
                     SVProgressHUD.dismiss()

                        break
                    case.failure(let error):
                        print(error.localizedDescription)
                    }
                }
    }
        
}
