//
//  AlertHelper.swift
//  GuideMe
//
//  Created by apple on 2/21/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit
import UserNotifications

var c_id = ""

var g_Id = ""

var notification = UNUserNotificationCenter.current()
var password = ""
var userOrGuider:Int = 0
var userId = 0
var guiderId = 0
class AlertHelper: NSObject {
    class func displayAlertMessage(messageToDisplay: String, sender: UIViewController)
    {
        let alertController = UIAlertController(title: "Alert", message: messageToDisplay, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default)
        {
            (actIon: UIAlertAction!) in
            print("Ok Button tapped")
        }
        alertController.addAction(OKAction)
        sender.present(alertController, animated: true, completion: nil)
    }
    
    class func isValidEmail(email:String?) -> Bool {
        guard email != nil else { return false }
        let regEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let pred = NSPredicate(format:"SELF MATCHES %@", regEx)
        return pred.evaluate(with: email)
    }
}


extension UIViewController{
    func hideKeyboard(){
        let tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard(){
        view.endEditing(true)
    }
}
